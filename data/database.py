import requests
from html2text import html2text
class Question():
    def __init__(self,question: str, correct_answer: bool) -> None:
        self.question = question
        self.correct_answer = correct_answer

class QuestionsDB():
    def __init__(self, url, params: dict = None) -> None:
        self._params = params
        self._questions: list[dict] = []
        self._url = url

        self._create_questions()

    def _create_questions(self):
        r = requests.get(self._url, self._params)
        r.raise_for_status()
        self._questions = r.json()["results"]

    def get_question(self,i):
        q = self._questions[i]

        return Question(html2text(q["question"]), True if q["correct_answer"] == "True" else False)
    