from customtkinter import CTkButton
from setting import FONT

class TrueBtn(CTkButton):
    def __init__(self, master, command):
        super().__init__(master)
        self.value = True
        self.configure(text ="True", font=FONT, command=command, fg_color="green", hover_color="#006400")
        self.reveal()

    def reveal(self):
        self.grid(row=2, column=0, padx=30, pady=25 )

    def hide(self):
        self.grid_forget()


class FalseBtn(CTkButton):
    def __init__(self, master, command):
        super().__init__(master)
        self.value = False
        self.configure(text ="False", font=FONT, command=command, fg_color="red", hover_color="#CD3333")
        self.reveal()

    def reveal(self):
        self.grid(row=2, column=1, padx=30, pady=25 )

    def hide(self):
        self.grid_forget()


class RestartQuiz(CTkButton):
    def __init__(self, master, command):
        super().__init__(master)
        self.configure(text ="Restart", font=FONT, command=command, anchor= "center")
        self.reveal()        
        
    def reveal(self):
        self.grid(row=2, column=0, padx=30, pady=25)

    def hide(self):
        self.grid_forget()


class SettingsBtn(CTkButton):
    def __init__(self, master, command):
        super().__init__(master)
        self.configure(text ="Settings", font=FONT, command=command, anchor= "center")
        self.reveal()
        
    def reveal(self):
        self.grid(row=2, column=1, padx=30, pady=25)

    def hide(self):
        self.grid_forget()


class ConfirmSettings(CTkButton):
    def __init__(self, master, command):
        super().__init__(master)
        self.configure(text ="Confirm", font=FONT, command=command, anchor= "center")
        self.reveal()
        
    def reveal(self):
        self.grid(row=2, column=0, padx=30, pady=25, columnspan=2)

    def hide(self):
        self.grid_forget()