from customtkinter import CTkFrame
from components.score_label import ScoreLabel

class ScoreFrame(CTkFrame):
    def __init__(self, master):
        super().__init__(master)

        self.grid(row = 0, column = 0, padx=50, pady=5, sticky="ew", columnspan=2)
        self.label = ScoreLabel(self)
        self.columnconfigure(0, weight=1)