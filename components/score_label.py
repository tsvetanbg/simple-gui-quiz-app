from customtkinter import CTkLabel
from setting import FONT

class ScoreLabel(CTkLabel):
    def __init__(self, master):
        super().__init__(master)

        self.score = 0
        self.configure(text = f"Score: {self.score}", font=FONT)
        self.grid(row=0, column=0)
    
    def increase_score(self):
        self.score += 1
        self.configure(text = f"Score: {self.score}")

    def restart_score(self):
        self.score = 0
        self.configure(text = f"Score: {self.score}")


