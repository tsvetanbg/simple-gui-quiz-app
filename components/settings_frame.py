from customtkinter import CTkFrame
import components.settings_labels as labels
import components.settings_options as options
import components.buttons as btns

class SettingsFrame(CTkFrame):
    def __init__(self, master):
        super().__init__(master)
        self.columnconfigure(0, weight=1)
        self.reveal()
        
        self.settings_label = labels.SettingsNameLabel(self)

        self.question_label = labels.QuestionsLabel(self)
        self.question_number = options.QuestionEntry(self)

        self.category_label = labels.CategoryLabel(self)
        self.category_options = options.CategoryOptions(self)

        self.difficulty_label = labels.DifficultyLabel(self)
        self.difficulty_options = options.DifficultyOptions(self)

        self.bind("<Enter>", self.on_hover)

    def on_hover(self, е):
        self.focus()
    
    def reveal(self):
        self.grid(row = 1, column = 0, padx=50, pady=100, sticky="eswn", columnspan=2)


    def hide(self):
        self.grid_forget()


    

