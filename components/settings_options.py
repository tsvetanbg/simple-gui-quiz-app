from customtkinter import CTkEntry, CTkOptionMenu
from setting import FONT
import requests

class QuestionEntry(CTkEntry):
    def __init__(self, master, ):
        super().__init__(master)

        self.configure(font=FONT, width=370)
        self.insert(0,10)
        self.bind("<Leave>", self.on_hover_out)
        self.bind("<1>", self.on_click)
        self.grid(row=1, column=1, pady=5, padx=20, sticky = "ew")
    
    def on_click(self, event):
        self.delete(0, len(self.get()))
        self.configure(text_color="black")

    def on_hover_out(self, event):
        if len(self.get()) < 1:
            self.insert(0,10)
            return
        try:
            int(self.get())
        except:
            self.delete(0, len(self.get()))
            self.insert(0,"Numbers only")
            self.configure(text_color = "red")

class CategoryOptions(CTkOptionMenu):
    def __init__(self,master):
        super().__init__(master)    

        self.api_url = "https://opentdb.com/api_category.php"

        self.categories: dict = self.get_categories()
        self.values = self.categories.keys()

        self.configure(font=FONT, values=self.values, width=370)
        self.set("Any Category")
        self.grid(row=2, column=1, pady=5, padx=20, sticky="ew")
        self.bind("<Enter>", self.on_hover)

    def on_hover(self,event):
        self.focus()

    def get_categories(self):
        r = requests.get(self.api_url)
        r.raise_for_status()
        
        categories: [dict] = r.json()["trivia_categories"]
        result: dict = {}
        for category in categories:

            name = category["name"]
            id = category["id"]
            
            result[name]= id

        return result

class DifficultyOptions(CTkOptionMenu):
    def __init__(self,master):
        super().__init__(master)

        self.values = [
            "Any Difficulty",
            "Easy",
            "Medium",
            "Hard"
        ]

        self.configure(font=FONT, values=self.values, width=370)
        self.set(self.values[0])
        self.grid(row=3, column=1, pady=5, padx=20, sticky="ew")
        self.bind("<Enter>", self.on_hover)

    def on_hover(self,event):
        self.focus()