from customtkinter import CTkFrame
from components.question_label import QuestionLabel
class QuestionFrame(CTkFrame):
    def __init__(self, master):
        super().__init__(master)
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0,weight=1)
        self.grid(row = 1, column = 0, padx=10, pady=10, sticky="eswn", columnspan= 2)
        self.label = QuestionLabel(self)
        self.def_color = self.cget("fg_color")

    def flash_color(self,color):
        self.configure(fg_color=color)
        self.after(350, self.set_def_colors)
        self.label.configure(text_color="white")

    def set_def_colors(self):
        self.configure(fg_color=self.def_color)
        self.label.configure(text_color="black")


