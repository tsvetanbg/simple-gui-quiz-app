from customtkinter import CTk
from components.question_frame import QuestionFrame
from components.score_frame import ScoreFrame
import components.buttons as btns
from data.database import QuestionsDB
from components.settings_frame import SettingsFrame


class App(CTk):
    def __init__(self):
        super().__init__()

        self.title("Fuzzy Quizzy")
        self.geometry("800x500")
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        self.load_settings()

    def _init_database(self, params):
        self._data = QuestionsDB("https://opentdb.com/api.php", params)
        self._question_index = 0
        self._current_question = self._data.get_question(self._question_index)

    def _install_first_question(self):
        q_label = self.question_frame.label
        q_label.configure(text=f"Question {self._question_index + 1}:\n{self._current_question.question}")

    def next_question(self):
        q_label = self.question_frame.label
        
        try:
            self._question_index += 1
            self._current_question = self._data.get_question(self._question_index)

            q_label.configure(text=f"Question {self._question_index + 1}:\n{self._current_question.question}")
            
        except IndexError:
            q_label.configure(text=f"FINISH.\n Your score is: {self.score_frame.label.score}")
            
            #Hide True/false buttons
            self.true_btn.hide()
            self.false_btn.hide()

            #install restart Button
            self.restart_btn = btns.RestartQuiz(self, self.restart_quiz)
            self.settings_btn = btns.SettingsBtn(self,self.load_settings)

    def increase_score(self):
        self.score_frame.label.increase_score()

    def check_answer(self, answer):
        if answer == self._current_question.correct_answer:
            self.question_frame.flash_color("green")
            self.increase_score()
        else:
            self.question_frame.flash_color("red")
        
        self.after(350, self.next_question)    

    def on_true_btn_click(self):
        self.check_answer(self.true_btn.value)

    def on_false_btn_click(self):
        self.check_answer(self.false_btn.value)

    def restart_quiz(self):
        self.load_quesitons()
        self.score_frame.label.restart_score()
        self.restart_btn.hide()

    def settings(self):
        self.after(1000, self.create_settings_frame)

    def create_settings_frame(self):
        self.question_frame.grid_forget()
        self.true_btn.hide()
        self.false_btn.hide()
        self.settings_btn.hide()

        self.settings_frame = SettingsFrame(self)
        self.confirm_settings_btn = btns.ConfirmSettings(self, self.load_quesitons)
    
    def load_quesitons(self):
        q_number = self.settings_frame.question_number.get()
        category = self.settings_frame.category_options.get()
        difficulty = self.settings_frame.difficulty_options.get()

        self.params = {"type": "boolean", "amount": q_number}

        if category != "Any Category":
            self.params["category"] = self.settings_frame.category_options.categories[category]

        if difficulty != "Any Difficulty":
            self.params["difficulty"] = difficulty.lower()

        self._init_database(self.params)

        self.settings_frame.hide()
        self.confirm_settings_btn.hide()
        
        self.score_frame = ScoreFrame(self)
        self.question_frame = QuestionFrame(self)
        
        self._install_first_question()

        self.true_btn = btns.TrueBtn(self, self.on_true_btn_click)
        self.false_btn = btns.FalseBtn(self, self.on_false_btn_click)
    
    def load_settings(self):
        self.settings_frame = SettingsFrame(self)
        self.confirm_settings_btn = btns.ConfirmSettings(self, self.load_quesitons)
        
        try:
            self.settings_btn.hide()
        except AttributeError:
            pass
        
        try:
            self.restart_btn.hide()
        except AttributeError:
            pass