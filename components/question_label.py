from customtkinter import CTkLabel
from setting import FONT

class QuestionLabel(CTkLabel):
    def __init__(self, master):
        super().__init__(master)

        self.configure( font=FONT, text="test", anchor="center", width=300, justify="center")

        self.grid(row=0, column=0)
    
