from customtkinter import CTkLabel
from setting import FONT

class SettingsNameLabel(CTkLabel):
    def __init__(self, master):
        super().__init__(master)

        self.configure(text = "SETTINGS", font=("Arial", 25, "bold"))
        self.grid(row=0, column=0,padx=5, pady=25, columnspan=2)

class QuestionsLabel(CTkLabel):
    def __init__(self, master):
        super().__init__(master)

        self.configure(text = "Number of Questions:", font=FONT)
        self.grid(row=1, column=0,padx=5, pady=5, sticky = "w")

class CategoryLabel(CTkLabel):
    def __init__(self, master):
        super().__init__(master)

        self.configure(text = "Select Category:", font=FONT)
        self.grid(row=2, column=0,padx=5, pady=5, sticky = "w")

class DifficultyLabel(CTkLabel):
    def __init__(self, master):
        super().__init__(master)

        self.configure(text = "Select Difficulty:", font=FONT)
        self.grid(row=3, column=0,padx=5, pady=5, sticky = "w")