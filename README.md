## Name
Simple GUI Quiz App

## Description
Fizzy Quizzy is a simple GUI application, based on Open Trivia Database (https://opentdb.com/).
Fizzy Quizzy have a settings menu, which will help you pick how many questions do you want to answer to, pick a category and difficulty. Also, the app will keep your score for every round, so you can see what is your end score.
Each time you restart the game or use "restart" button, the App will make new call to the Trivia database and will get new quesitons. 
DISCLAMER! You can get the same question on the next run, because the app does not use Session token and does not keep your answered question.

#Requierments
Python 3.10 or above

certifi==2023.11.17
charset-normalizer==3.3.2
customtkinter==5.2.2
darkdetect==0.8.0
html2text==2020.1.16
idna==3.6
packaging==23.2
requests==2.31.0
urllib3==2.1.0

## Installation
1. Clone this repository, by following the Gitlab instructions.
2. You can create virtual environment, so you keep the project in its own scope.
    - Open your desired folder.
    - Use "python3 -m venv ." or "python -m venv ." to create virtual environment in the same folder.
    - Activate the virtual envioronment.
    - Install the requirements for the project: "pip install -r requirements.txt" or "pip3 install -r requirements.txt"
3. You can run the app with command "python main.py" or "python3 main.py". Make sure that you activate the venv and you are in the correct folder(root folder of the project).
4. Enjoy.

Additionally, you can use module like "pyinstaller" to create a .exe, if you want to use it as a program.

## Support
For additional quesitons, suggestions or reporting a bug please, contact me on tsvetanbg@gmail.com

## License
Use as you want. I won't change anyone or any company, which want to use the app.

## Project status
Finished.
If any suggestions how to upgrade the project feel free to contact me on tsvetanbg@gmail.com